#include "stm32f10x.h"                  // Device header
#include "Delay.h"

int main(void)
{
	/*
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);	//开启GPIOA的时钟
															//使用各个外设前必须开启时钟，否则对外设的操作无效
	
	
	GPIO_InitTypeDef GPIO_InitStructure;					//定义结构体变量
	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;		//GPIO模式，赋值为推挽输出模式
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_All;				//GPIO引脚，赋值为所有引脚
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;		//GPIO速度，赋值为50MHz
	
	GPIO_Init(GPIOA, &GPIO_InitStructure);					//将赋值后的构体变量传递给GPIO_Init函数
															//函数内部会自动根据结构体的参数配置相应寄存器
															//实现GPIOA的初始化
	GPIO_Init(GPIOB, &GPIO_InitStructure);					//将赋值后的构体变量传递给GPIO_Init函数
															//函数内部会自动根据结构体的参数配置相应寄存器
															//实现GPIOA的初始化
	
	
	while (1)
	{
		
		
		
		GPIO_Write(GPIOA, ~0x0001);	//0000 0000 0000 0001，PA0引脚为低电平，其他引脚均为高电平，注意数据有按位取反
		Delay_ms(500);				//延时500ms
		GPIO_Write(GPIOA, ~0x0002);	//0000 0000 0000 0010，PA1引脚为低电平，其他引脚均为高电平
		Delay_ms(500);				//延时500ms
		GPIO_Write(GPIOA, ~0x0004);	//0000 0000 0000 0100，PA2引脚为低电平，其他引脚均为高电平
		Delay_ms(500);				//延时500ms
		GPIO_Write(GPIOA, ~0x0008);	//0000 0000 0000 1000，PA3引脚为低电平，其他引脚均为高电平
		Delay_ms(500);				//延时500ms
		GPIO_Write(GPIOA, ~0x0010);	//0000 0000 0001 0000，PA4引脚为低电平，其他引脚均为高电平
		Delay_ms(500);				//延时500ms
		GPIO_Write(GPIOA, ~0x0020);	//0000 0000 0010 0000，PA5引脚为低电平，其他引脚均为高电平
		Delay_ms(500);				//延时500ms
		GPIO_Write(GPIOA, ~0x0040);	//0000 0000 0100 0000，PA6引脚为低电平，其他引脚均为高电平
		Delay_ms(500);				//延时500ms
		GPIO_Write(GPIOA, ~0x0080);	//0000 0000 1000 0000，PA7引脚为低电平，其他引脚均为高电平
		Delay_ms(500);				//延时500ms
		
		
	}
	*/
	
	
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);	//开启GPIOB的时钟
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);	//开启GPIOB的时钟
															//使用各个外设前必须开启时钟，否则对外设的操作无效
	
	
	GPIO_InitTypeDef GPIO_InitStructure;					//定义结构体变量
	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;		//GPIO模式，赋值为推挽输出模式
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_All;				//GPIO引脚，赋值为所有引脚
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;		//GPIO速度，赋值为50MHz
	
	GPIO_Init(GPIOA, &GPIO_InitStructure);	
	GPIO_Init(GPIOB, &GPIO_InitStructure);					//将赋值后的构体变量传递给GPIO_Init函数
															//函数内部会自动根据结构体的参数配置相应寄存器
															//实现GPIOB的初始化
	
	
	while (1)
	{
		//PB组阵脚
		
		GPIO_ResetBits(GPIOB, GPIO_Pin_1);		//将PB1引脚设置为低电平，蜂鸣器鸣叫
		Delay_ms(500);							//延时500ms
		GPIO_SetBits(GPIOB, GPIO_Pin_1);		//将PB1引脚设置为高电平，蜂鸣器停止
		Delay_ms(500);	
		GPIO_ResetBits(GPIOB, GPIO_Pin_8);		//将PB8引脚设置为低电平，蜂鸣器鸣叫
		Delay_ms(500);							//延时500ms
		GPIO_SetBits(GPIOB, GPIO_Pin_8);		//将PB8引脚设置为高电平，蜂鸣器停止
		Delay_ms(500);							//延时500ms
		
		GPIO_ResetBits(GPIOB, GPIO_Pin_9);		//将PB9引脚设置为低电平，蜂鸣器鸣叫
		Delay_ms(500);							//延时500ms
		GPIO_SetBits(GPIOB, GPIO_Pin_9);		//将PB9引脚设置为高电平，蜂鸣器停止
		Delay_ms(500);							//延时500ms
		
		GPIO_ResetBits(GPIOB, GPIO_Pin_10);		//将PB10引脚设置为低电平，蜂鸣器鸣叫
		Delay_ms(500);							//延时100ms
		GPIO_SetBits(GPIOB, GPIO_Pin_10);		//将PB10引脚设置为高电平，蜂鸣器停止
		Delay_ms(500);							//延时500ms
		
		GPIO_ResetBits(GPIOB, GPIO_Pin_11);		//将PB11引脚设置为低电平，蜂鸣器鸣叫
		Delay_ms(500);							//延时500ms
		GPIO_SetBits(GPIOB, GPIO_Pin_11);		//将PB11引脚设置为高电平，蜂鸣器停止
		Delay_ms(500);							//延时500ms
		
		GPIO_ResetBits(GPIOB, GPIO_Pin_12);		//将PB12引脚设置为低电平，蜂鸣器鸣叫
		Delay_ms(500);							//延时500ms
		GPIO_SetBits(GPIOB, GPIO_Pin_12);		//将PB12引脚设置为高电平，蜂鸣器停止
		Delay_ms(500);							//延时100ms
		
		GPIO_ResetBits(GPIOB, GPIO_Pin_13);		//将PB13引脚设置为低电平，蜂鸣器鸣叫
		Delay_ms(500);							//延时500ms
		GPIO_SetBits(GPIOB, GPIO_Pin_13);		//将PB13引脚设置为高电平，蜂鸣器停止
		Delay_ms(500);							//延时500ms
		
		GPIO_ResetBits(GPIOB, GPIO_Pin_14);		//将PB14引脚设置为低电平，蜂鸣器鸣叫
		Delay_ms(500);							//延时500ms
		GPIO_SetBits(GPIOB, GPIO_Pin_14);		//将PB14引脚设置为高电平，蜂鸣器停止
		Delay_ms(500);							//延时500ms
		
		GPIO_ResetBits(GPIOB, GPIO_Pin_15);		//将PB15引脚设置为低电平，蜂鸣器鸣叫
		Delay_ms(500);							//延时500ms
		GPIO_SetBits(GPIOB, GPIO_Pin_15);		//将PB15引脚设置为高电平，蜂鸣器停止
		Delay_ms(500);							//延时500ms
		
		
		//PA组阵脚
		GPIO_ResetBits(GPIOA, GPIO_Pin_0);		//将PA0引脚设置为低电平，蜂鸣器鸣叫
		Delay_ms(500);							//延时500ms
		GPIO_SetBits(GPIOA, GPIO_Pin_0);		//将PA0引脚设置为高电平，蜂鸣器停止
		Delay_ms(500);							//延时500ms
		
		GPIO_ResetBits(GPIOA, GPIO_Pin_1);		//将PA1引脚设置为低电平，蜂鸣器鸣叫
		Delay_ms(500);							//延时500ms
		GPIO_SetBits(GPIOA, GPIO_Pin_1);		//将PA1引脚设置为高电平，蜂鸣器停止
		Delay_ms(500);							//延时500ms
		
		GPIO_ResetBits(GPIOA, GPIO_Pin_2);		//将PA2引脚设置为低电平，蜂鸣器鸣叫
		Delay_ms(500);							//延时500ms
		GPIO_SetBits(GPIOA, GPIO_Pin_2);		//将PA2引脚设置为高电平，蜂鸣器停止
		Delay_ms(500);							//延时500ms
		
		GPIO_ResetBits(GPIOA, GPIO_Pin_3);		//将PA3引脚设置为低电平，蜂鸣器鸣叫
		Delay_ms(500);							//延时500ms
		GPIO_SetBits(GPIOA, GPIO_Pin_3);		//将PA3引脚设置为高电平，蜂鸣器停止
		Delay_ms(500);							//延时500ms
		
		GPIO_ResetBits(GPIOA, GPIO_Pin_4);		//将PA4引脚设置为低电平，蜂鸣器鸣叫
		Delay_ms(500);							//延时500ms
		GPIO_SetBits(GPIOA, GPIO_Pin_4);		//将PA4引脚设置为高电平，蜂鸣器停止
		Delay_ms(500);	
		GPIO_ResetBits(GPIOA, GPIO_Pin_5);		//将PA5引脚设置为低电平，蜂鸣器鸣叫
		Delay_ms(500);							//延时500ms
		GPIO_SetBits(GPIOA, GPIO_Pin_5);		//将PA5引脚设置为高电平，蜂鸣器停止
		Delay_ms(500);	
		GPIO_ResetBits(GPIOA, GPIO_Pin_6);		//将PA6引脚设置为低电平，蜂鸣器鸣叫
		Delay_ms(500);							//延时500ms
		GPIO_SetBits(GPIOA, GPIO_Pin_6);		//将PA6引脚设置为高电平，蜂鸣器停止
		Delay_ms(500);			
		GPIO_ResetBits(GPIOA, GPIO_Pin_7);		//将PA7引脚设置为低电平，蜂鸣器鸣叫
		Delay_ms(500);							//延时500ms
		GPIO_SetBits(GPIOA, GPIO_Pin_7);		//将PA7引脚设置为高电平，蜂鸣器停止
		Delay_ms(500);		
			
	}
	
}
